APP=n26

test:
	go test -cover .

build: clean
	go build -o ${APP}

docker-build:
	docker build -t ${APP} .

run:
	go run main.go helpers.go handlers.go server.go

docker-run: docker-build
	docker run -p 127.0.0.1:53:8053 -p 127.0.0.1:53:8053/udp --rm ${APP}

clean:
	go clean
