package main

import (
	"crypto/tls"
	"fmt"
	"io"
)

const (
	defultUpstream = "8.8.8.8:853"
)

type upstream struct {
	address string
	config  *tls.Config
}

func newUpstream(address string) *upstream {
	if address == "" {
		address = defultUpstream
	}
	return &upstream{address: address}
}

func (u *upstream) request(req []byte) ([]byte, error) {
	conn, err := tls.Dial("tcp", u.address, u.config)
	if err != nil {
		return nil, fmt.Errorf("failed to connect: %w", err.Error())
	}
	defer conn.Close()

	_, err = conn.Write(withPrefix(req))
	if err != nil {
		return nil, fmt.Errorf("can't write the request: %w", err.Error())
	}

	resp, err := readMessage(conn)
	if err != nil {
		return nil, fmt.Errorf("can't read the response: %w", err.Error())
	}

	return resp, nil
}

// Returns prefix based on lenght (https://tools.ietf.org/html/rfc1035#section-4.2.2)
func encodePrefix(l int) []byte {
	prefix := make([]byte, 2)
	prefix[0] = byte(l >> 8)
	prefix[1] = byte(l)

	return prefix
}

// Returns message length based on prefix (https://tools.ietf.org/html/rfc1035#section-4.2.2)
func decodePrefix(p []byte) int {
	return int(p[0])<<8 | int(p[1])
}

func withPrefix(message []byte) []byte {
	prefix := encodePrefix(len(message))

	return append(prefix, message...)
}

func readMessage(conn io.Reader) ([]byte, error) {
	var message []byte

	// Get message length (https://tools.ietf.org/html/rfc1035#section-4.2.2)
	prefix := make([]byte, 2)
	_, err := io.ReadFull(conn, prefix)
	if err != nil {
		return nil, err
	}
	l := decodePrefix(prefix)

	message = make([]byte, l)
	_, err = io.ReadFull(conn, message)
	if err != nil {
		return nil, err
	}

	return message, nil
}
