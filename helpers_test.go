package main

import (
	"bytes"
	"reflect"
	"testing"

	"golang.org/x/net/dns/dnsmessage"
)

func TestDecodePrefix(t *testing.T) {
	prefix := []byte{
		byte(7),
		byte(228),
	}

	length := decodePrefix(prefix)
	if length != 2020 {
		t.Error("length is not correct", length)
	}
}

func TestEncodePrefix(t *testing.T) {
	length := 1337
	prefix := encodePrefix(length)
	if prefix[0] != 5 && prefix[1] != 57 {
		t.Errorf("prefix[0]: %d, prefix[1]: %d", prefix[0], prefix[1])
	}
}

func TestWithPrefix(t *testing.T) {
	msg := make([]byte, 512)
	packet := withPrefix(msg)
	length := decodePrefix(packet)
	if length != 512 {
		t.Errorf("Message length is incorrect")
	}
}

func TestReadMessage(t *testing.T) {
	msg := make([]byte, 512)
	//Fill message with random numbers
	msg[13] = byte(4)
	conn := bytes.NewBuffer(withPrefix(msg))

	res, err := readMessage(conn)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(msg, res) {
		t.Error("messages are not equal")
	}
}

//TestUpstreamRequest generates a new request, sends it to upstream server and parses an answer
func TestUpstreamRequest(t *testing.T) {
	msg := dnsmessage.Message{
		Header: dnsmessage.Header{Response: false},
		Questions: []dnsmessage.Question{
			{
				Name:  dnsmessage.MustNewName("google.com."),
				Type:  dnsmessage.TypeA,
				Class: dnsmessage.ClassINET,
			},
		},
	}

	req, err := msg.Pack()
	if err != nil {
		t.Error(err)
	}

	us := newUpstream("")
	resp, err := us.request(req[2:])
	if err != nil {
		t.Error(err)
	}

	var m dnsmessage.Message
	err = m.Unpack(resp)
	if err != nil {
		t.Error(err)
	}
}
