package main

import (
	"net"
)

type Server struct {
	us *upstream
}

func NewServer(us *upstream) *Server {
	return &Server{us: us}
}

func (s *Server) ServeTCP(address string) error {
	ln, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}
		go func(c net.Conn) {
			s.handleTCP(c)
			c.Close()
		}(conn)
	}
}

func (s *Server) ServeUDP(address string) error {
	conn, err := net.ListenPacket("udp", address)
	if err != nil {
		return err
	}
	defer conn.Close()

	buf := make([]byte, 512)
	for {
		n, addr, err := conn.ReadFrom(buf)
		if err != nil {
			return err
			continue
		}

		go s.handleUDP(conn, addr, buf[:n])
	}
}
