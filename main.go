package main

import (
	"log"
	"os"
)

func main() {
	listen := os.Getenv("LISTEN")
	if listen == "" {
		listen = ":8053"
	}

	us := newUpstream(os.Getenv("UPSTREAM"))
	s := NewServer(us)

	go func() {
		err := s.ServeUDP(listen)
		if err != nil {
			log.Fatal(err)
		}
	}()

	log.Fatal(s.ServeTCP(listen))
}
