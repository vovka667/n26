# How to run

## Docker

```bash
make test
make docker-run
```

## Without docker

You will need Golang installed
```bash
make test
LISTEN=":53" make run
```

# Environment variables

This appliaction supports following varibles

* `LISTEN` - ip/host and port where to bind. ":8053" is default.
* `UPSTREAM` - ip/host and port of upstream DoT server. "8.8.8.8:853" is default

# What would I impove in the project

* decode/encode messages. It allows us to log queries. And do something more if needed. Like monitoring, checking and so on.
* use multiple upstream servers for reliability.
* reuse upstream connections, but not open a new one every time. Pool of connection is also a good idea.
* send and receive messages asynchronously
* run embedded DoT server in tests for offline testing. Docker compose is also an option.
* return adequate rcodes in case of error
* add more tests :)

# How would I run it in the real world

* I would run at least 2 instances in every datacenter/cloud project and usem them as deafult resolvers
* depending on security requirements plain DNS connecction could be disabled by default in firewall/security groups
* use CAP_NET_BIND_SERVICE to bind to port 53 without root privs
