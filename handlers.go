package main

import (
	"io"
	"log"
	"net"
)

func (s *Server) handleTCP(conn net.Conn) {
	for {
		req, err := readMessage(conn)
		if err == io.EOF {
			log.Println("no more data to read. bye!")
			return
		} else if err != nil {
			log.Printf("can't read the request: %s", err.Error())
			return
		}

		resp, err := s.us.request(req)
		if err != nil {
			log.Printf("can't read the response: %s", err.Error())
			return
		}

		_, err = conn.Write(withPrefix(resp))
		if err != nil {
			log.Printf("can't write the response: %s", err.Error())
			return
		}
	}
}

func (s *Server) handleUDP(conn net.PacketConn, addr net.Addr, req []byte) {
	resp, err := s.us.request(req)
	if err != nil {
		log.Printf("upstream returns an error: %s", err.Error())
		return
	}
	_, err = conn.WriteTo(resp, addr)
	if err != nil {
		log.Printf("can't write the response: %s", err.Error())
	}
}
